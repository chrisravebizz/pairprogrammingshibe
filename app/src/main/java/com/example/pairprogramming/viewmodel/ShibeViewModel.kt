package com.example.pairprogramming.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pairprogramming.model.ShibeRepo
import com.example.pairprogramming.view.shibe.ShibeState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShibeViewModel @Inject constructor(
    private val repo: ShibeRepo
) : ViewModel() {

    //    private val repo by lazy { ShibeRepo }
    private val _state = MutableLiveData(ShibeState(isLoading = true))
    val state: LiveData<ShibeState> get() = _state

    init {
        viewModelScope.launch {
            val dogs = repo.getShibes()
            _state.value = ShibeState(shibes = dogs)
        }
    }


}