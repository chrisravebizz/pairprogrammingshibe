package com.example.pairprogramming.view.shibe

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.example.pairprogramming.R
import com.example.pairprogramming.databinding.FragmentShibeBinding
import com.example.pairprogramming.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class ShibeFragment : Fragment() {

    private var _binding: FragmentShibeBinding? = null
    private val binding get() = _binding
    private val shibeViewModel by viewModels<ShibeViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shibeViewModel.state.observe(viewLifecycleOwner, object : Observer<ShibeState>{
            override fun onChanged(t: ShibeState?) {
                t?.shibes?.let {
                    binding?.rvImages?.adapter = ShibeAdapter(t.shibes)
                }
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}