package com.example.pairprogramming.view.shibe

class ShibeState(
    val isLoading: Boolean = false,
    val shibes: ArrayList<String> = arrayListOf()
)