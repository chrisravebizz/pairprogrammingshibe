package com.example.pairprogramming.view.shibe

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import coil.ImageLoader
import coil.load
import com.example.pairprogramming.databinding.ItemCardBinding

class ShibeAdapter(
    private val urls: List<String>
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) =
        holder.bindShibes(urls[position])

    override fun getItemCount() = urls.size


    class ShibeViewHolder(
        private val binding: ItemCardBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindShibes(url: String) {
            binding.ivDogs.load(url)
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemCardBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { ShibeViewHolder(it) }
        }
    }
}
