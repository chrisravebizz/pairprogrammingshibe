package com.example.pairprogramming

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShibeApplication: Application()