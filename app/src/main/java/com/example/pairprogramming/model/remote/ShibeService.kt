package com.example.pairprogramming.model.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface ShibeService {

    companion object {
        private const val BASE_URL = "https://shibe.online"
        private const val QUERY_COUNT = "count"

        fun getInstance() = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<ShibeService>()
    }

    @GET("/api/shibes")
    suspend fun getShibes(@Query(QUERY_COUNT) id : Int = 100) : ArrayList<String>
}