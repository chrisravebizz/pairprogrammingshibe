package com.example.pairprogramming.model

import com.example.pairprogramming.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.http.Query
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ShibeRepo @Inject constructor(
    private val shibeService: ShibeService
) {

//    private val shibeService by lazy { ShibeService.getInstance() }

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        shibeService.getShibes()
    }

}